---
title: 7/9 INDEPENDENT WORK
date: 2017-07-09
---

#### Moodboard
Ik heb besloten dat ik niet genoeg foto's heb verzameld voor ons specifieke thema. Daarom ben ik nogmaals naar Rotterdam gereisd om foto's te maken die te maken hebben met _Muziek & Rotterdam_ . Vervolgens heb ik deze foto's uitgewerkt tot een moodboard. Hiervoor heb ik de foto's gegroepeerd in de volgende groepen.

 - Muziekwinkels
 - Muziekposters
 - Bars & Café's
 - Muzikale Streetart
 - Overige muzikale & algemene interessante locaties.
