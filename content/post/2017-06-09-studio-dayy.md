---
title: 6/9 INDEPENDENT WORK
date: 2017-06-09
---
#### Moodboard
Een van de eerste deliverables die ik af moest hebben was een moodboard. Daarom ben ik vandaag voor school samen met mijn team de stad in getrokken om foto's te maken voor onze moodboards. We hebben foto's gemaakt van bezienswaardigheden in de stad.
